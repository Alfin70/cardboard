const myDatabase = [
    {name:"Alfin Kerin", email:"alfin@gmail.com", age:"25"},
    {name:"Firhat Sungkar", email:"firhat@gmail.com", age:"29"},
    {name:"Rina", email:"Rina@gmail.com", age:"23"},
    {name:"Rizki", email:"rizki@gmail.com", age:"21"},
];

(function kartuNama(db){

    const init = function(){
        generateList();
        enterUser();
    }

  const generateList = function(){
        let parent = document.querySelector('#bapaknya');
        let template = '';

        for(let i=0; i<db.length; i++){

            template +='<div class="col-sm-4">';
            template +=' <div class="card">';
            template +=' <div class="card-delete "data-card="'+ i +'">X</div>';
            template +='<div class="card-block">'; 
            template +='<h4 class="card-title">'+db[i].name+' </h4>';
            template +=' <p class="card-text">';
            template +=' <strong>Email</strong>:<span>'+db[i].email+'</span>';
            template +='</p>';
            template +=' <p class="card-text">';
            template +=' <strong>age</strong>:<span>'+db[i].age+'</span> </p>';
            template +='</div>';
            template +='</div>';
            template +='</div>';

        }
       parent.innerHTML = '';
       parent.insertAdjacentHTML('afterbegin', template);     
  }

  const enterUser = function(){
      function grabUser(){
          const name = document.querySelector('#user_name').Value;
          const email = document.querySelector('#user_email').Value;
          const age = document.querySelector('#user_age').Value;

            const final = [name,email,age];
            console.log(final)
      }
    document.querySelector('#myForm').addEventListener("submit", function(event){
        event.preventDefault();
        grabUser();
    })

  }

  init();
})(myDatabase)